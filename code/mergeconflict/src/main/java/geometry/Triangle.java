package geometry;

public class Triangle {
    private double side1;
    private double side2;
    private double side3;
    private double height;
    
    public Triangle(double side1, double side2, double side3, double height){
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.height = height;
    }

    public double getSide1(){
        return this.side1;
    }
    public double getSide2(){
        return this.side2;
    }
    public double getSide3(){
        return this.side3;
    }
    public double height(){
        return this.height;
    }

    public String toString(){
        return "a tringle wwith sides of this lengths" + this.side1 + " " + this.side2 + " " + this.side3 + " and a height of" + this.height;
    }

}