package geometry;

public class Square {
    private double side;
    
    public Square(double side){
        this.side = side;
    }

    public double getSide(){
        return this.side;
    }

    public double getArea(){
        return this.side * this.side;
    }

    public String toString(){
        return "A square of side " + getSide() + " has an area of " + getArea();
    }
}
